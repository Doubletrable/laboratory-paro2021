#include <iostream>
#include <string>
#include "scrable.hpp"
using namespace std;
int scramble_points(string word){
    int points=0;
    char c;
    for(const auto b:word){
        c = toupper(b);
        if(c =='A' || c=='E' || c=='I' || c=='O' || c=='U' || c=='L' || c=='N' || c=='R' || c=='S' || c=='T'){
            points += 1;
        }
        else if(c== 'D' || c=='G') {
            points += 2;
        }
        else if(c =='B' || c =='C' || c =='M' || c =='P'){
            points += 3;
        }
        else if(c =='F' || c =='H' || c =='V' || c =='W' || c == 'Y'){
            points += 4;
        }
        else if(c=='K') {
            points += 5;
        }
        else if(c== 'J' || c=='X') {
            points += 7;
        }
    else if(c== 'Q' || c=='Z') {
            points += 10;
        }
    }
    return points;
}
