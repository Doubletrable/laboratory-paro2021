add_executable(scrableTest
  scrable-test.cpp)

target_link_libraries (scrableTest PUBLIC scrable gtest_main)

add_test(NAME scrable
    COMMAND scrableTest --gtest_output=xml:${CMAKE_BINARY_DIR}/junit/exercise1.junit.xml)
