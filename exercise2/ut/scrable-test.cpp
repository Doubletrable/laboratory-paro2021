#include "gtest/gtest.h"
#include "scrable.hpp"

struct ScrableTestSuite {};


TEST(ScrableTestSuite, OneLetterWord)
{
  ASSERT_EQ(scramble_points("A"), 1);
}

TEST(ScrableTestSuite, MyCabbages)
{
  ASSERT_EQ(scramble_points("CABBAGE" ), 14);
}
TEST(ScrableTestSuite, Mycabbages)
{
  ASSERT_EQ(scramble_points("cabbage" ), 14);
}
TEST(ScrableTestSuite, Myname)
{
  ASSERT_EQ(scramble_points("KamIL" ), 11);
}
TEST(ScrableTestSuite, Myothername)
{
  ASSERT_EQ(scramble_points("KaMILa" ), 12);
}
