#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};


TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, March)
{
  ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}
TEST(DayOfYearTestSuite, April)
{
  ASSERT_EQ(dayOfYear(4, 25, 1940), 116);
}
TEST(DayOfYearTestSuite, June)
{
  ASSERT_EQ(dayOfYear(6, 27, 1953), 178);
}
TEST(DayOfYearTestSuite, September)
{
  ASSERT_EQ(dayOfYear(9, 3, 2017), 246);
}
TEST(DayOfYearTestSuite, December)
{
  ASSERT_EQ(dayOfYear(12, 15, 2000), 350);
}

